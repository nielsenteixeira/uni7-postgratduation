package com.uni7.posgrad.posgraduni7.converter;

import com.uni7.posgrad.posgraduni7.model.ZipCode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.convert.ConversionService;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ConverterTest {

    @Autowired
    ConversionService conversionService;

    @Test
    public void when_convert_string_to_cep_then_success() {

        ZipCode actualZipCode = new ZipCode("60123", "999");
        ZipCode convertedZipCode = conversionService.convert("60123-999", ZipCode.class);

        assertThat(convertedZipCode)
                .isEqualToComparingFieldByField(actualZipCode);
    }

    @Test
    public void when_convert_cep_to_string_then_success() {
        String actualZipCode = "60123-999";

        String convertedZipCode = conversionService
                .convert(new ZipCode("60123", "999"), String.class);

        assertThat(convertedZipCode)
                .isEqualTo(actualZipCode);

    }
}
