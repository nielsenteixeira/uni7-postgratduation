package com.uni7.posgrad.posgraduni7.exception;

import com.uni7.posgrad.posgraduni7.model.Hangman;

public class InsufficientAttemptsException extends RuntimeException {
    public InsufficientAttemptsException(String message) {
        super(message);
    }
}