package com.uni7.posgrad.posgraduni7.model;

import javax.persistence.Entity;

@Entity
public class ZipCode extends BaseEntity {
    private String region;
    private String suffix;

    public ZipCode(String region, String suffix) {
        this.region = region;
        this.suffix = suffix;
    }

    public ZipCode() {
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }
}
