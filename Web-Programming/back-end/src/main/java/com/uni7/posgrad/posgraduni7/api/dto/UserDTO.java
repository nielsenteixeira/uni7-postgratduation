package com.uni7.posgrad.posgraduni7.api.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.uni7.posgrad.posgraduni7.api.converter.CepToStringConverter;
import com.uni7.posgrad.posgraduni7.api.converter.StringToCepConverter;
import com.uni7.posgrad.posgraduni7.api.validator.BRZipCode;
import com.uni7.posgrad.posgraduni7.model.User;
import org.modelmapper.ModelMapper;

import java.util.Date;

public class UserDTO {
    private Long id;
        private String citizenId;
    private String name;
    private String username;
    private String password;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date date;
    @BRZipCode
    private String zipCode;

    public UserDTO () { }

    public UserDTO(User user) {
        setId(user.getId());
        setCitizenId(user.getCitizenId());
        setName(user.getName());
        setUsername(user.getUsername());
        setPassword(user.getPassword());
        setDate(user.getDate());
        setZipCode(new CepToStringConverter().convert(user.getZipCode()));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }



    public User toUser(){
        ModelMapper modelMapper = new ModelMapper();
        var user = modelMapper.map(this, User.class);

        var zipCode = new StringToCepConverter().convert(getZipCode());
        user.setZipCode(zipCode);
        return user;
    }
}
