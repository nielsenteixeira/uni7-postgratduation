package com.uni7.posgrad.posgraduni7.service;

import com.uni7.posgrad.posgraduni7.exception.EntityNotFoundException;
import com.uni7.posgrad.posgraduni7.model.User;
import com.uni7.posgrad.posgraduni7.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User save(User user){
        return userRepository.save(user);
    }

    public Optional<User> findById(long id){
        return userRepository.findById(id);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public void deleteById(long id){
        userRepository.deleteById(id);
    }

    public User Update(Long id, User user) {
        if(!userRepository.existsById(id)) {
            throw new EntityNotFoundException(User.class);
        }

        var existingUser = userRepository.findById(id).get();
        existingUser.setName(user.getName());
        existingUser.setZipCode(user.getZipCode());
        existingUser.setDate(user.getDate());
        existingUser.setPassword(user.getPassword());
        existingUser.setCitizenId(user.getCitizenId());

        return userRepository.save(existingUser);
    }

    public Optional<User> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }
}
