package com.uni7.posgrad.posgraduni7.api;

import com.uni7.posgrad.posgraduni7.api.advice.ApiError;
import com.uni7.posgrad.posgraduni7.api.dto.UserDTO;
import com.uni7.posgrad.posgraduni7.exception.EntityNotFoundException;
import com.uni7.posgrad.posgraduni7.model.User;
import com.uni7.posgrad.posgraduni7.service.UserService;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/users")
public class UserController {
    private final UserService userService;
    private final ConversionService conversionService;

    public UserController(UserService userService, ConversionService conversionService) {
        this.userService = userService;
        this.conversionService = conversionService;
    }

    @GetMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public UserDTO findById(@PathVariable long id){
        return userService.findById(id)
                .map(UserDTO::new)
                .orElseThrow(() -> new EntityNotFoundException(User.class));
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<UserDTO> getAll(){
        return userService
                .findAll()
                .stream()
                .map(UserDTO::new)
                .collect(Collectors.toList());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> saveUser(@RequestBody @Valid UserDTO userDTO, Errors errors){
        if (errors.hasErrors()) {
            return ResponseEntity.badRequest().body(new ApiError(HttpStatus.BAD_REQUEST, errors));
        }

        var savedUser = userService.save(userDTO.toUser());
        return ResponseEntityBuilder.created(savedUser.getId());
    }

    @PutMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> updateUser(@RequestBody @Valid UserDTO userDTO, @PathVariable("id") Long id, Errors errors){
        if (errors.hasErrors()) {
            return ResponseEntity.badRequest().body(new ApiError(HttpStatus.BAD_REQUEST, errors));
        }

        var savedUser = userService.Update(id, userDTO.toUser());
        return ResponseEntity.ok(new UserDTO(savedUser));
    }

    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable long id){
        var user = userService.findById(id)
                .orElseThrow(()-> new EntityNotFoundException(User.class));

        userService.deleteById(user.getId());
    }
}
