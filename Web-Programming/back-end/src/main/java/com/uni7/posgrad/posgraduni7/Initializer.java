package com.uni7.posgrad.posgraduni7;

import com.uni7.posgrad.posgraduni7.model.User;
import com.uni7.posgrad.posgraduni7.model.ZipCode;
import com.uni7.posgrad.posgraduni7.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.Date;

@Configuration
public class Initializer {

    private final UserService userService;

    public Initializer(UserService userService) {
        this.userService = userService;
        init();
    }

    public void init(){
        var user = new User();
        user.setCitizenId("11111111111");
        user.setPassword("123456");
        user.setDate(new Date());
        user.setName("Admin");
        user.setUsername("admin");
        var zipCode = new ZipCode();
        zipCode.setSuffix("60000");
        zipCode.setRegion("999");
        user.setZipCode(zipCode);

        userService.save(user);
    }
}
