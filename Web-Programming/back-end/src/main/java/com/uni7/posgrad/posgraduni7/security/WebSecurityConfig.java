package com.uni7.posgrad.posgraduni7.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.csrf().disable().authorizeRequests()
			.antMatchers(HttpMethod.OPTIONS, "/auth").permitAll()
			.antMatchers(HttpMethod.POST, "/auth").permitAll().antMatchers(HttpMethod.POST, "/login").permitAll()

			.antMatchers(HttpMethod.POST, "/hangman/start").permitAll()
			.antMatchers(HttpMethod.OPTIONS, "/hangman/start").permitAll()
			.antMatchers(HttpMethod.POST, "/hangman/character").permitAll()
			.antMatchers(HttpMethod.OPTIONS, "/hangman/character").permitAll()
			.antMatchers(HttpMethod.POST, "/hangman/shot").permitAll()
			.antMatchers(HttpMethod.OPTIONS, "/hangman/shot").permitAll()

			.antMatchers(HttpMethod.GET, "/users").permitAll()
			.antMatchers(HttpMethod.POST, "/users").permitAll()
			.antMatchers(HttpMethod.PUT, "/users/**").permitAll()
			.antMatchers(HttpMethod.DELETE, "/users/**").permitAll()
			.antMatchers(HttpMethod.GET, "/users/**").permitAll()
			.antMatchers(HttpMethod.OPTIONS, "/users").permitAll()
			.antMatchers(HttpMethod.OPTIONS, "/users/**").permitAll()

			.antMatchers(HttpMethod.GET, "/cars").permitAll()
			.antMatchers(HttpMethod.POST, "/cars").permitAll()
			.antMatchers(HttpMethod.PUT, "/cars/**").permitAll()
			.antMatchers(HttpMethod.DELETE, "/cars/**").permitAll()
			.antMatchers(HttpMethod.GET, "/cars/**").permitAll()
			.antMatchers(HttpMethod.OPTIONS, "/cars").permitAll()
			.antMatchers(HttpMethod.OPTIONS, "/cars/**").permitAll()
			.anyRequest().authenticated();
			//.and()
			

			
			// filtra outras requisições para verificar a presença do JWT no header
//			.addFilterBefore(new JWTAuthenticationFilter(),
//	                UsernamePasswordAuthenticationFilter.class);
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		var encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
	    // cria uma conta default
		auth.inMemoryAuthentication()
			.withUser("admin")
			.password(encoder.encode("123456"))
			.roles("ADMIN");
	}
}