package com.uni7.posgrad.posgraduni7.api;

import com.uni7.posgrad.posgraduni7.api.dto.AuthDTO;
import com.uni7.posgrad.posgraduni7.api.dto.AuthResponse;
import com.uni7.posgrad.posgraduni7.exception.InvalidUserException;
import com.uni7.posgrad.posgraduni7.security.JWTService;
import com.uni7.posgrad.posgraduni7.security.TokenAuthenticationService;
import com.uni7.posgrad.posgraduni7.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/auth")
public class AuthController {

    private final UserService userService;

    public AuthController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity auth(@RequestBody @Valid AuthDTO authDTO) throws Exception{
        var user = userService.findByUsername(authDTO.getLogin())
                .orElseThrow(() -> new InvalidUserException("invalid username or password"));

        return TokenAuthenticationService.addAuthentication(user.getUsername(), HttpStatus.OK);
    }
}
