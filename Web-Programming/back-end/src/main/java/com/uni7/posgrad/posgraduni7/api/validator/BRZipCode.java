package com.uni7.posgrad.posgraduni7.api.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ZipCodeNumberValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface BRZipCode {
    String message() default "Invalid zip code. Zip code must be in format #####-### and only numbers.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}