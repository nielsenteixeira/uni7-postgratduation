package com.uni7.posgrad.posgraduni7.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

public class ResponseEntityBuilder {

    public static ResponseEntity<?> created(Long id) {
        var location = ServletUriComponentsBuilder.fromCurrentRequest()
                                                  .path("/{id}")
                                                  .buildAndExpand(id)
                                                  .toUri();

        return ResponseEntity
                .created(location)
                .body(id);
    }
}