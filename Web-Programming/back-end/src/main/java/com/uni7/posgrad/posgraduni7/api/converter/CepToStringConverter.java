package com.uni7.posgrad.posgraduni7.api.converter;

import com.uni7.posgrad.posgraduni7.model.ZipCode;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CepToStringConverter implements Converter<ZipCode, String> {

    @Override
    public String convert(ZipCode zipCode) {
        return zipCode.getSuffix() + "-" + zipCode.getRegion();
    }
}
