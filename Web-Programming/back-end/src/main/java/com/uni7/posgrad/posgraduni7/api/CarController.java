package com.uni7.posgrad.posgraduni7.api;

import com.uni7.posgrad.posgraduni7.exception.EntityNotFoundException;
import com.uni7.posgrad.posgraduni7.model.Car;
import com.uni7.posgrad.posgraduni7.service.CarService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "/cars")
public class CarController {
    private final CarService carService;

    public CarController(CarService carService) {
        this.carService = carService;
    }

    @GetMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Car findById(@PathVariable long id){
        return carService.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(Car.class));
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Car> getAll(){
        return carService.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> saveCar(@RequestBody Car car){
        var savedCar = carService.save(car);
        return ResponseEntityBuilder.created(savedCar.getId());
    }

    @PutMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> updateCar(@RequestBody @Valid Car car, @PathVariable("id") Long id, Errors errors){
        var savedCar = carService.save(car);
        return ResponseEntityBuilder.created(savedCar.getId());
    }

    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable long id){
        carService.deleteById(id);
    }
}
