package com.uni7.posgrad.posgraduni7.exception;

public class GameWasFinishedException extends RuntimeException {
    public GameWasFinishedException(String message) {
        super(message);
    }
}