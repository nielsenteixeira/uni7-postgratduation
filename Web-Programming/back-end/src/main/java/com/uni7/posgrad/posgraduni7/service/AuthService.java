package com.uni7.posgrad.posgraduni7.service;

import com.uni7.posgrad.posgraduni7.exception.EntityNotFoundException;
import com.uni7.posgrad.posgraduni7.exception.InvalidUserException;
import com.uni7.posgrad.posgraduni7.model.User;
import com.uni7.posgrad.posgraduni7.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException;

import java.util.List;
import java.util.Optional;

@Service
public class AuthService {
    private final UserService userService;

    public AuthService(UserService userService) {
        this.userService = userService;
    }

    public User autenticarUsuario(String username, String password) throws InvalidUserException {
        var user = userService.findByUsername(username)
                .orElseThrow(()-> new InvalidUserException("invalid username or password."));

        if(user.getPassword() != password) {
            throw  new InvalidUserException("invalid username or password.");
        }

        return user;
    }
}
