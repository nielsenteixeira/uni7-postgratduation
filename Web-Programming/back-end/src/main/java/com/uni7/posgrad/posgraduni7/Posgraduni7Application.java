package com.uni7.posgrad.posgraduni7;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Posgraduni7Application {

	public static void main(String[] args) {
		SpringApplication.run(Posgraduni7Application.class, args);
	}

}
