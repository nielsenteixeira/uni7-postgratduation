package com.uni7.posgrad.posgraduni7.api.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ZipCodeNumberValidator implements
        ConstraintValidator<BRZipCode, String> {
 
    @Override
    public void initialize(BRZipCode brZipCode) {
    }
 
    @Override
    public boolean isValid(String zipCode, ConstraintValidatorContext cxt) {
        return zipCode != null && zipCode.matches("[0-9]{5}-[0-9]{3}");
    }
 
}