package com.uni7.posgrad.posgraduni7.repository;

import com.uni7.posgrad.posgraduni7.model.Hangman;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface HangmanRepository extends JpaRepository<Hangman, Long> {
    Optional<Hangman> findByIdentifier(UUID identifier);
}
