package com.uni7.posgrad.posgraduni7.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Car extends BaseEntity {

    @Column(unique = true)
    private String plate;
    private String owner;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date plateDate;
    private Long tax;

    public Car(){}

    public Car(String plate, String owner, Date plateDate, Long tax) {
        this.plate = plate;
        this.owner = owner;
        this.plateDate = plateDate;
        this.tax = tax;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Date getPlateDate() {
        return plateDate;
    }

    public void setPlateDate(Date plateDate) {
        this.plateDate = plateDate;
    }

    public Long getTax() {
        return tax;
    }

    public void setTax(Long tax) {
        this.tax = tax;
    }
}
