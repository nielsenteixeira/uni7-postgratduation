package com.uni7.posgrad.posgraduni7.service;

import com.uni7.posgrad.posgraduni7.exception.EntityNotFoundException;
import com.uni7.posgrad.posgraduni7.model.Car;
import com.uni7.posgrad.posgraduni7.repository.CarRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CarService {

    private final CarRepository carRepository;

    public CarService(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    public List<Car> findAll() {
        return carRepository.findAll();
    }

    public Car save(Car car){
        return carRepository.save(car);
    }

    public Car update(Car carToUpdate){
        if(!carRepository.existsById(carToUpdate.getId())) {
            throw new EntityNotFoundException(Car.class);
        }

        var existingCar = carRepository.findById(carToUpdate.getId()).get();
        existingCar.setOwner(carToUpdate.getOwner());
        existingCar.setPlate(carToUpdate.getPlate());
        existingCar.setPlateDate(carToUpdate.getPlateDate());
        existingCar.setTax(carToUpdate.getTax());

        return carRepository.save(existingCar);
    }

    public void deleteById(Long id){
        carRepository.deleteById(id);
    }

    public Optional<Car> findById(Long id){
        return carRepository.findById(id);
    }
}
