package com.uni7.posgrad.posgraduni7.repository;

import com.uni7.posgrad.posgraduni7.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
}
