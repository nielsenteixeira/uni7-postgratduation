package com.uni7.posgrad.posgraduni7.repository;

import com.uni7.posgrad.posgraduni7.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<Car, Long> {
}
