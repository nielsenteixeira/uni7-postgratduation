package com.uni7.posgrad.posgraduni7.api;

import com.uni7.posgrad.posgraduni7.api.dto.HunchDTO;
import com.uni7.posgrad.posgraduni7.model.Hangman;
import com.uni7.posgrad.posgraduni7.service.HangmanService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "/hangman")
public class HangmanGameController {

    private final HangmanService hangmanService;

    public HangmanGameController(HangmanService hangmanService) {
        this.hangmanService = hangmanService;
    }

    @PostMapping(path = "/start")
    @ResponseStatus(HttpStatus.OK)
    public Hangman startGame() {
        return hangmanService.startGame();
    }

    @PostMapping(path = "/character")
    @ResponseStatus(HttpStatus.OK)
    public Hangman existsLetter(@RequestBody @Valid HunchDTO hunchDTO) {
        return hangmanService.existsCharacter(hunchDTO.getCharacter(), hunchDTO.getIdentifier());
    }

    @PostMapping(path = "/shot")
    @ResponseStatus(HttpStatus.OK)
    public Hangman tryShot(@RequestBody HunchDTO hunchDTO) {
        return hangmanService.tryHit(hunchDTO.getShot(), hunchDTO.getIdentifier());
    }
}
