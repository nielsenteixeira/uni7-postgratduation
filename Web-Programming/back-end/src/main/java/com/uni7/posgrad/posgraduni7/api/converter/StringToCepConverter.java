package com.uni7.posgrad.posgraduni7.api.converter;

import com.uni7.posgrad.posgraduni7.model.ZipCode;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class StringToCepConverter implements Converter<String, ZipCode> {

    @Override
    public ZipCode convert(String from) {
        var data = from.split("-");
        return new ZipCode(data[0], data[1]);
    }
}
