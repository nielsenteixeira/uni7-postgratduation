class CarrosService {
    constructor() {
        this.axios = axios.create({
            baseURL: 'http://localhost:9000/cars'
        });
        axios.defaults.headers.post['Content-Type'] ='application/json;charset=utf-8';
        axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
    }
    request(method, url, data) {
        return this.axios[method](url, data)
            .then(response => {
                console.log(response)
                return response.data;
            })
            .catch(error => {
                console.error('[ERROR: CarrosService] ' + method + ' ' + url, error);
                return Promise.reject(error);
            });
    }
    selecionar(id) {
        return this.request('get', '/' + id);
    }
    selecionarTodos() {
        return this.request('get');
    }
    inserir(carro) {
        return this.request('post', '', carro);
    }
    atualizar(carro) {
        return this.request('put', '/' + carro.id, carro);
    }
    excluir(id) {
        return this.request('delete', '/' + id);
    }
}

var carrosService = new CarrosService();