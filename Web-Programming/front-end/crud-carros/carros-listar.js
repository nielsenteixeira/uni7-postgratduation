var app = new Vue({
    el: '#app',
    data: {
        mensagem: {},
        headers: [{
                text: 'ID',
                sortable: true,
                value: 'id'
            },
            {
                text: 'Nome',
                sortable: true,
                value: 'owner'
            },
            {
                text: 'Placa',
                sortable: true,
                value: 'plate'
            },
            {
                text: 'Data Emplacamento',
                sortable: true,
                value: 'plateDate'
            },,
            {
                text: 'IPVA',
                sortable: true,
                value: 'tax'
            },
        ],
        carros: [],
    },
    mounted() {
        this.carregar();
    },
    methods: {
        carregar() {
            this.limparMensagem();
            carrosService.selecionarTodos()
                .then(carros => {
                    this.carros = carros;
                })
                .catch(error => {
                    this.carros = [];
                    this.exibirMensagem('error', 'Erro inesperado.');
                });
        },
        exibirMensagem(tipo, texto) {
            this.mensagem = {
                tipo,
                texto,
                exibir: true
            };
        },
        limparMensagem() {
            this.mensagem = {
                tipo: 'info',
                texto: '',
                exibir: false
            };
        },
        editar(id) {
            window.location = 'carros-editar.html?id=' + id;
        },
    }
});