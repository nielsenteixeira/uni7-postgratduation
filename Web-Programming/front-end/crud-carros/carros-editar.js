var router = new VueRouter({
    mode: 'history',
    routes: []
});
var app = new Vue({
    router,
    el: '#app',
    data: {
        mensagem: {},
        car: {
            id: null,
            plate: null,
            owner: null,
            plateDate: null,
            tax: null,
        },
        errors: {
            plate: null,
            owner: null,
            plateDate: null,
            tax: null,
        }
    },
    mounted() {
        this.car.id = this.$route.query.id;
        this.carregar();
    },
    methods: {
        modoEdicao() {
            return this.car.id ? true : false;
        },
        inserir() {
            this.limparMensagem();
            carrosService.inserir(this.car)
                .then(id => {
                    this.car.id = id;
                    this.exibirMensagem('success',
                        'Carro com id = ' + id + ' criado com sucesso.');
                })
                .catch(error => {
                    this.tratarErro(error);
                });
        },
        tratarErro(error) {
            switch (error.response.status) {
                case 404: // Not found
                    this.exibirMensagem('error', 'O registro solicitado não foi encontrado!');
                    break;
                default:
                    this.exibirMensagem('error', 'Erro inesperado.');
                    break;
            }
        },
        exibirMensagem(tipo, texto) {
            this.mensagem = {
                tipo,
                texto,
                exibir: true
            };
        },
        limparMensagem() {
            this.mensagem = {
                tipo: 'info',
                texto: '',
                exibir: false
            };
        },
        carregar() {
            if (this.modoEdicao()) {
                carrosService.selecionar(this.car.id)
                    .then(car => {
                        this.car = car;
                    })

                    .catch(error => {
                        this.tratarErro(error);
                    });

            }
        },
        atualizar() {
            this.limparMensagem();
            carrosService.atualizar(this.car)
                .then(data => {
                    this.exibirMensagem('success', 'Carro atualizado com sucesso.');
                })
                .catch(error => {
                    this.tratarErro(error);
                });
        },
        excluir() {
            this.limparMensagem();
            carrosService.excluir(this.car.id)
                .then(data => {
                    this.car = {
                        id: null,
                        plate: null,
                        owner: null,
                        plateDate: null,
                        tax: null,
                    };
                    this.exibirMensagem('success', 'Carro excluído com sucesso.');
                })
                .catch(error => {
                    this.tratarErro(error);
                });
        },
    },
});