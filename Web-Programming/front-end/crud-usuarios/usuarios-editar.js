var router = new VueRouter({
    mode: 'history',
    routes: []
});
var app = new Vue({
    router,
    el: '#app',
    data: {
        mensagem: {},
        user: {
            id: null,
            citizenId: null,
            name: null,
            zipCode: null,
            date: null,
            password: null,
        },
        errors: {
            citizenId: null,
            name: null,
            zipCode: null,
            date: null,
            password: null,
        }
    },
    mounted() {
        this.user.id = this.$route.query.id;
        this.carregar();
    },
    methods: {
        modoEdicao() {
            return this.user.id ? true : false;
        },
        inserir() {
            this.limparMensagem();
            usuariosService.inserir(this.user)
                .then(id => {
                    this.user.id = id;
                    this.exibirMensagem('success',
                        'Usuário com id = ' + id + ' criado com sucesso.');
                })
                .catch(error => {
                    this.tratarErro(error);
                });
        },
        tratarErro(error) {
            switch (error.response.status) {
                case 404: // Not found
                    this.exibirMensagem('error', 'O registro solicitado não foi encontrado!');
                    break;
                default:
                    this.exibirMensagem('error', 'Erro inesperado.');
                    break;
            }
        },
        exibirMensagem(tipo, texto) {
            this.mensagem = {
                tipo,
                texto,
                exibir: true
            };
        },
        limparMensagem() {
            this.mensagem = {
                tipo: 'info',
                texto: '',
                exibir: false
            };
        },
        carregar() {
            if (this.modoEdicao()) {
                usuariosService.selecionar(this.user.id)
                    .then(user => {
                        this.user = user;
                    })

                    .catch(error => {
                        this.tratarErro(error);
                    });

            }
        },
        atualizar() {
            this.limparMensagem();
            usuariosService.atualizar(this.user)
                .then(data => {
                    this.exibirMensagem('success', 'Usuário atualizado com sucesso.');
                })
                .catch(error => {
                    this.tratarErro(error);
                });
        },
        excluir() {
            this.limparMensagem();
            usuariosService.excluir(this.user.id)
                .then(data => {
                    this.user = {
                        id: null,
                        citizenId: null,
                        name: null,
                        zipCode: null,
                        date: null,
                        password: null,
                    };
                    this.exibirMensagem('success', 'Usuário excluído com sucesso.');
                })
                .catch(error => {
                    this.tratarErro(error);
                });
        },
    },
});