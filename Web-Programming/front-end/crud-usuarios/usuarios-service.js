class UsuariosService {
    constructor() {
        this.axios = axios.create({
            baseURL: 'http://localhost:9000/users'
        });
        axios.defaults.headers.post['Content-Type'] ='application/json;charset=utf-8';
        axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
    }
    request(method, url, data) {
        return this.axios[method](url, data)
            .then(response => {
                console.log(response)
                return response.data;
            })
            .catch(error => {
                console.error('[ERROR: UsuariosService] ' + method + ' ' + url, error);
                return Promise.reject(error);
            });
    }
    selecionar(id) {
        return this.request('get', '/' + id);
    }
    selecionarTodos() {
        return this.request('get');
    }
    inserir(usuario) {
        return this.request('post', '', usuario);
    }
    atualizar(usuario) {
        return this.request('put', '/' + usuario.id, usuario);
    }
    excluir(id) {
        return this.request('delete', '/' + id);
    }
}

var usuariosService = new UsuariosService();