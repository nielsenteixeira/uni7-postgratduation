## Projeto
Projeto para disciplina de Programação para Web do curso de Pós graduação do Centro universitátio 7 de setembro.

### Back-end
Back end desenvolvido em JAVA versão 11 com spring boot.
Para armazenagem de dados foi utilizado o banco de dados H2.

#### Setup

##### Prerequisitos
- [OpenJDK 11](http://jdk.java.net/11/)
- [Maven 3.6+](https://maven.apache.org/install.html) or [IntelliJ IDEA](https://www.jetbrains.com/help/idea/install-and-set-up-product.html)

##### Estrutura do projeto:

```
src
├── main 
│   └── java
│   |   └── com.uni7.posgrad.posgraduni7
│   |       └── api (Controllers)
│   |       |   └── advice (Tratamento de erros)
│   |       |   └── converter (Conversores)
│   |       |   └── dto (Modelos para transferencia de dados)
│   |       |   └── validator (Validadores)
│   |       └── config (Classes de configuração)
│   |       └── exception (Exceções customizadas)
│   |       └── model (Modelos de negócio)
│   |       └── repository (Repositórios)
│   |       └── security (Classes de segurança e JWT)
│   |       └── service (Classes de serviço)
│   └── resources
└ pom.xml
```

Para iniciar o projeto executar o comando:

```
mvn clean compile spring-boot:run
```

Métodos do CRUD de Usuários

```
GET /users
GET /users/{id}
POST /users
PUT /users/{id}
DELETE /users/{id}
```

Modelo de usuário
```json
{
    "id": 1,
    "citizenId": "11111111111",
    "name": "Admin",
    "username": "admin",
    "password": "123456",
    "date": "17/03/2019",
    "zipCode": "60000-999"
}

```

Métodos do CRUD de Carros

```
GET /cars
GET /cars/{id}
POST /cars
PUT /cars/{id}
DELETE /cars/{id}
```

Modelo de carro
```json
{
    "id": 1,
    "plate": "NSL-9628",
    "owner": "Nielsen Costa Teixeira",
    "plateDate": "01/01/2001",
    "tax": 1000
}

```

____________________________________________________________

### Front-end
Projeto desenvolvido com [Vue JS](https://vuejs.org/) e [Vuetifyjs](https://vuetifyjs.com/pt-BR/)

##### Prerequisitos
- [NodeJS](https://nodejs.org/en/)
- [NPM](https://www.npmjs.com/)

Para os cruds de carro e usuário segui o mesmo exemplo dado em sala de aula.
* Crud de usuários: Web-Programming/front-end/crud-usuarios/index.html
* Crud de usuários: Web-Programming/front-end/crud-carros/index.html

Para a autenticação JWT e jogo da forca trabalhei com projeto padrão Vue com Vue CLI 

Para iniciar o projeto executar o comando:


```
npm install
```
```
npm run serve
```

Path login:

```
/auth
```
Path jogo da velha:

```
/hangman
```